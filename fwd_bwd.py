states = ('Rain', 'Not Rain')
end_state = 'E'

observations = ('Umbrella','Umbrella','Not Umbrella','Umbrella','Umbrella')

start_probability = {'Rain': 0.6, 'Not Rain': 0.4}

transition_probability = {
   'Rain' : {'Rain': 0.69, 'Not Rain': 0.3, end_state:0.01},
   'Not Rain' : {'Not Rain': 0.69, 'Rain': 0.3, end_state:0.01},
   }

emission_probability = {
   'Rain' : {'Umbrella': 0.9, 'Not Umbrella': 0.1},
   'Not Rain' : {'Umbrella': 0.2, 'Not Umbrella': 0.8},
   }


def fwd_bkw(x, states, a_0, a, e, end_st):
    L = len(x)
 
    fwd = []
    f_prev = {}
    # forward part of the algorithm
    for i, x_i in enumerate(x):
        f_curr = {}
        for st in states:
            if i == 0:
                # base case for the forward part
                prev_f_sum = a_0[st]
            else:
                prev_f_sum = sum(f_prev[k]*a[k][st] for k in states)
 
            f_curr[st] = e[st][x_i] * prev_f_sum
 
        fwd.append(f_curr)
        f_prev = f_curr
 
    p_fwd = sum(f_curr[k]*a[k][end_st] for k in states)
 
    bkw = []
    b_prev = {}
    # backward part of the algorithm
    for i, x_i_plus in enumerate(reversed(x[1:]+(None,))):
        b_curr = {}
        for st in states:
            if i == 0:
                # base case for backward part
                b_curr[st] = a[st][end_st]
            else:
                b_curr[st] = sum(a[st][l]*e[l][x_i_plus]*b_prev[l] for l in states)
 
        bkw.insert(0,b_curr)
        b_prev = b_curr
 
    p_bkw = sum(a_0[l] * e[l][x[0]] * b_curr[l] for l in states)
 
    # merging the two parts
    posterior = []
    for i in range(L):
        posterior.append({st: fwd[i][st]*bkw[i][st]/p_fwd for st in states})

    assert p_fwd == p_bkw
    return fwd, bkw, posterior

def example():
    return fwd_bkw(observations,
                   states,
                   start_probability,
                   transition_probability,
                   emission_probability,
                   end_state)

fw,bw,ps=example()
for k in fw:
    print k
print

for k in bw:
    print k
print

for k in ps:
    print k
print
